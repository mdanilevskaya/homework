﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
namespace maski_modif
{
    class MaskSearchAndReplace
    {
        public static void Take(DateTime Sborder, DateTime Fborder, string mask, string Rword, string directory)
        {

          //тут поиск файлов типа тхт во всех папках и под,
            string[] DirInfo = Directory.GetFiles(directory,"*.txt",SearchOption.AllDirectories);
            //тут цикл все пути к файлам тхт записаны в масиве  диринфо я буду проверять каждый потом записывать из файла в стринг и чето делать
            for (int i = 0; i < DirInfo.Length; i++)
            {
                StreamReader Input = new StreamReader(DirInfo[i]);
                FileInfo file = new FileInfo(DirInfo[i]);
               
                string buffer = "";
                string buf;
                if (file.CreationTime > Sborder || file.CreationTime < Fborder)
                    while (( buf=Input.ReadLine()) != null)
                    {
                       
                        buffer += buf + "\n";
                    }
                Regex regular = new Regex(mask);
                Match match = regular.Match(buffer);
                //тут ищем и меняем маску на текст
                if (file.CreationTime > Sborder && file.CreationTime < Fborder)
                {
                    while (match.Success == true)
                    {
                        buffer = regular.Replace(buffer, Rword);
                        match = match.NextMatch();
                    }
                    Input.Close();
                    StreamWriter Output = new StreamWriter(DirInfo[i]);
                    Output.WriteLine(buffer);
                    Output.Close();
                    Console.WriteLine("File " + DirInfo[i] + " Ok!");
                   
                }



            }
            Console.WriteLine("Replacing in all files done!");
            }
    }
}
