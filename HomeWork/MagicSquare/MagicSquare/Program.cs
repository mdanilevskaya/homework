﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyProgram
{
    class Magic
    {
        static void change(int[,] magik, int i1, int j1, int i2, int j2)
        {
            magik[i1, j1] -= magik[i2, j2];
            magik[i2, j2] += magik[i1, j1];
            magik[i1, j1] -= magik[i2, j2];
            magik[i1, j1] = -magik[i1, j1];
        }
        static void Main(string[] args)
        {
            int i = 0;
            int j = 0;

            Console.Write("Введите ранг матрицы (от 3 до 5) : ");
            int n = Convert.ToInt32(Console.ReadLine());

            int[,] magik = new int[7, 7];
            if (n == 4) //Если ранг равен 4, то действуем по частному случаю метода Рауз-Болла
            {
                int k = 0;
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < n; j++)
                    {
                        k++;
                        magik[i, j] = k;
                    }
                }
                //Заполнили массив по порядку, теперь преобразовываем к магическому виду
                change(magik, 0, 0, 3, 3);
                change(magik, 1, 1, 2, 2);
                change(magik, 0, 3, 3, 0);
                change(magik, 2, 1, 1, 2);
                {
                    Console.WriteLine("Ваш магический квадрат: ");
                    for (i = 0; i < n; i++)
                    {
                        Console.WriteLine("{0,3} {1,3} {2,3} {3,3}", magik[i, 0], magik[i, 1], magik[i, 2], magik[i, 3]);
                    }
                }
            }
            else
            {
                for (i = 0; i < n; i++)
                    for (j = 0; j < n; j++)
                        magik[i, j] = 0; //заполняем массив нулями

                int k = 1;
                i = 0; j = 1;
                magik[i, j] = k;
                for (k = 2; k <= n * n; k++)
                {
                    i += 2; j += 2;
                    do
                    {
                        if (i >= n) i -= n;
                        if (j >= n) j -= n;
                        if (magik[i, j] != 0)
                        {
                            i += 1;
                            j += 3;
                        }
                    } while ((i >= n) || (j >= n) || (magik[i, j] != 0));
                    magik[i, j] = k;
                }
                if (n == 5)
                {
                    Console.WriteLine("Ваш магический квадрат: ");
                    for (i = 0; i < n; i++)
                        Console.WriteLine("{0,3} {1,3} {2,3} {3,3} {4,3}", magik[i, 0], magik[i, 1], magik[i, 2], magik[i, 3], magik[i, 4]);
                }
                else
                {
                    Console.WriteLine("Ваш магический квадрат: ");
                    for (i = 0; i < n; i++)

                        Console.WriteLine("{0,3} {1,3} {2,3}", magik[i, 0], magik[i, 1], magik[i, 2]);
                }
            }
            Console.ReadKey();
        }
    }
}