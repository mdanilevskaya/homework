﻿using System;
namespace ConsoleApplication1
{
    public delegate double Function(double x);           // объявление делегата

    class Program
    {
        public static void Table(Function F, double x, double b)
        {
            Console.WriteLine(" ----- Градус ----- Значение  --");
            while (x <= b)
            {
                Console.WriteLine("| {0,8:F0}      | {1,8:F2}     |", x, F(x));
                x += 1;
            }
            Console.WriteLine(" -------------------------------");
        }


        static void Main()
        {
            Console.WriteLine(" Таблица функции Sin ");
            Table(new Function(Math.Sin), 1, 10);

            Console.WriteLine(" Таблица функции Cos ");
            Table(Math.Cos, 1, 10);


            Console.WriteLine(" Таблица функции Tg ");
            Table(delegate(double x) { return Math.Tan(x); }, 1, 10);
            Console.ReadKey();

        }
    }
}
