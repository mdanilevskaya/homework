﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Birth
{
    class MyBirth
    {
        public int year, month, day;
        public MyBirth(int y, int m, int d)
        {
            year = y;
            month = m;
            day = d;
        }
        public void Rezult()
        {
            DateTime firstDate = new DateTime(year, month, day);
            DateTime lastDate = DateTime.Today;
            TimeSpan span = lastDate - firstDate;
            double result = span.TotalDays;
            Console.WriteLine("Сегодняшняя дата: {0}", lastDate);
            Console.WriteLine("Количество прожитых дней: {0} ", result);
        }
    }
}
