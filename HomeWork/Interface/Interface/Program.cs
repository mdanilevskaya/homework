﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using mylcass_namespace;
using myspace = myclass_namespace;


class B : myspace.Inter
{
    public void method()
    {
        Console.WriteLine("B");
    }
}

class C : B
{
    public void method()
    {
        Console.WriteLine("C");
    }
}


class Program
{
    static void myQ(myspace.Inter number)
    {
        number.method();
    }

    static void Main(string[] args)
    {
        B b = new B();
        C c = new C();
        // c.www();
        myQ(b);
        myQ(c);


        Console.ReadKey();

    }
}