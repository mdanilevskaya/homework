﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyRoad
{


    public class Road
    {
        public TS[] ts = new TS[4];
        public Road()
        {
            Truck truck = new Truck();
            ts[0] = truck;
            ts[0].Violator = true;

            Bicycle bicycle = new Bicycle();
            ts[1] = bicycle;
            ts[1].Violator = true;

            Car car = new Car();
            ts[2] = car;
            ts[2].Violator = false;


            Motorcycle motorcycle = new Motorcycle();
            ts[3] = motorcycle;
            ts[3].Violator = false;
        }
    }


    public class TS
    {
        public Boolean Violator;
        public virtual int check()
        {
            return 0;
        }
    }

    public class Truck : TS
    {
        public Boolean n = true;

        public override int check()
        {
            Console.WriteLine("Грузовик остановлен!");
            return 0;
        }
    }


    public class Car : TS
    {
        public Boolean n = true;
        public override int check()
        {
            Console.WriteLine("Машина остановлена!");
            return 0;
        }
    }


    public class Bicycle : TS
    {
        public Boolean n = true;
        public override int check()
        {
            Console.WriteLine("Велосипед остановлен!");
            return 0;
        }
    }


    public class Motorcycle : TS
    {
        public Boolean n = false;
        public override int check()
        {
            Console.WriteLine("Мотоцикл остановлен!");
            return 0;
        }
    }


    public class Police
    {
        public int fine(Road road)
        {
            for (int i = 0; i <= 3; i++)
            {
                if (road.ts[i].Violator == true)
                {
                    road.ts[i].check();
                }
            }
            return 0;
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            Road road = new Road();
            Police police = new Police();
            police.fine(road);
            Console.ReadLine();
        }
    }
}
