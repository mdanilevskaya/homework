﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ConsoleApplication1
{
    class SortInsertArray
    {
        public class Array //класс, который содержит методы для работы с массивом
        {
            public int size;
            public int[,] arr;
            public int i,j,k,m;

            public Array(int K)
            {
                size = K;
            }

            public void Init()//инициализация массива случайными числами
            {
                arr = new int[size,size];
                Random r = new Random();

                for (i = 0; i < size; i++)
                    for (j = 0; j < size; j++)
                    {
                        int с = r.Next(-50, 50);
                        arr[i, j] = с;

                    }
            }


            public void Showarr()//метод вывода массива на экран
            {
                for (i = 0; i < size; i++)
                {
                    for (j = 0; j < size; j++)
                    {
                        Console.Write("{0,5}", arr[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

            public void SortInsert()   //сортировка массива 
            {
                for (i = 0; i < size; i++)
                    for (j = 0; j < size; j++)          //Buble sort
                    {
                        for (k = 0; k < size; k++)
                            for (m = 0; m < size; m++)
                            {
                                if (arr[i, j] < arr[k, m])   // Если элементы не по порядку-меняем местами
                                {
                                    int temp = arr[k, m];
                                    arr[k, m] = arr[i, j];
                                    arr[i, j] = temp;
                                }
                            }
                    }
            }

            public void Showsortarr()//вывод на экран отсортированного массива
            {
                
                Console.WriteLine("Ваш отсортированный массив:");
                for (i = 0; i < size; i++)
                {
                    for (j = 0; j < size; j++)
                    {
                        Console.Write("{0,5}", arr[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

        }

        static void Main(string[] args)
        {

            try //исключение
            {
                Console.Write("Введите размерность квадратной матрицы: ");
                int size = Convert.ToInt32(Console.ReadLine());

                Array array = new Array(size);
                array.Init();
                array.Showarr();
                array.SortInsert();               
                array.Showsortarr();


                Console.ReadLine();
            }

            catch (FormatException)
            {
                Console.WriteLine("Это НЕ число!!!\n");
                Console.ReadLine();
                return;
            }


        }
    }
}
