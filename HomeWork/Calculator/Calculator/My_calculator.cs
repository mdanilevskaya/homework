﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
   public class MyCalculator
    {
       private int First;
       private int Second;
       
       public MyCalculator (int First_,int Second_)
       {
          First = First_;
          Second = Second_;
       }
       
       public int Sum ()                  // метод сложения
       {
           return First + Second;
       }

       public int Sub()                    // метод вычитания
       {
           return First - Second;
       }

       public double Div()                // метод деления
       {
           return First / Second;
       }

       public double Mul ()             // метод умножения
       {
           return First * Second;
       }
    }
}
