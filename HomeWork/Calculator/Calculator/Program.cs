﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int First = 0, Second = 0;

            try
            {
                Console.Write("Input first: ");
                First = Convert.ToInt32(Console.ReadLine());

                Console.Write("Input second: ");
                Second = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            MyCalculator MyCalc = new MyCalculator(First, Second);
            Console.Write("Sum: {0}\n",MyCalc.Sum());
            Console.Write("Subtract: {0}\n", MyCalc.Sub());
            Console.Write("Divide: {0}\n", MyCalc.Div());
            Console.Write("Multiply: {0}", MyCalc.Mul());
            Console.ReadKey();
        }
    }
}
